from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
# from django.conf.urls.media import media
from django.conf import settings

# from persona.views import VistaBautiso

urlpatterns = [
    # Examples:
    # url(r'^$', 'secretaria.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    # url(r'^$', include(admin.site.urls)),
    url(r'^admin/', include(admin.site.urls)),

    ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT)


admin.site.site_header = 'Sistema Parroquia San Luis Rey de Francia'
