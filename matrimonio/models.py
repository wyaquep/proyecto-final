# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from persona.models import Persona

class Matrimonio(models.Model):
    libro = models.IntegerField('No. Libro')
    folio = models.IntegerField('No. Folio')
    persona = models.OneToOneField(Persona)
    sacerdote = models.CharField(
        'Padre quien bendijo Matrimonio', max_length=15)
    fechamat = models.DateField('Fecha del Matrimonio')
    nombrepadrino = models.CharField('Nombres del Padrino', max_length=30)
    apellidopadrino = models.CharField('Apellidos del Padrino', max_length=30)
    nombremadrina = models.CharField('Nombres de Madrina', max_length=30)
    apellidomadrina = models.CharField('Apellidos de Madrina', max_length=30)

    def __unicode__(self):
        return '%s  %s' % (self.nombrepadrino, self.nombremadrina)

    class Meta:
        db_table = 'matrimonio'
        verbose_name = 'Registro de Matrimonio'
        verbose_name_plural = 'Registro de Matrimonios'
