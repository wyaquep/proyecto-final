# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.core.files.storage import FileSystemStorage
from django.db import models
from persona.models import Persona

# fs = FileSystemStorage(location='actas/')

class Bautismo(models.Model):
    libro = models.IntegerField('No. Libro')
    folio = models.IntegerField('No. Folio')
    persona = models.OneToOneField(Persona)
    fechabaut = models.DateField('Fecha del Bautismo')
    nombrepadrino = models.CharField('Nombres del Padrino', max_length=30)
    apellidopadrino = models.CharField('Apellidos del Padrino', max_length=30)
    nombremadrina = models.CharField('Nombres de Madrina', max_length=30)
    apellidomadrina = models.CharField('Apellidos de Madrina', max_length=30)
    padrebautizo = models.CharField('Padre quien Bautizo', max_length=15)
    subirdoc = models.FileField(upload_to='documentos')

    def __unicode__(self):
        return '%s' % (self.persona)

    class Meta:
        db_table = 'bautismo'
        verbose_name = 'Registro de Bautismo'
        verbose_name_plural = 'Registro de Bautismos'
