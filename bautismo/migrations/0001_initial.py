# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('persona', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bautismo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('libro', models.IntegerField(verbose_name='No. Libro')),
                ('folio', models.IntegerField(verbose_name='No. Folio')),
                ('fechabaut', models.DateField(verbose_name='Fecha del Bautismo')),
                ('nombrepadrino', models.CharField(max_length=30, verbose_name='Nombres del Padrino')),
                ('apellidopadrino', models.CharField(max_length=30, verbose_name='Apellidos del Padrino')),
                ('nombremadrina', models.CharField(max_length=30, verbose_name='Nombres de Madrina')),
                ('apellidomadrina', models.CharField(max_length=30, verbose_name='Apellidos de Madrina')),
                ('padrebautizo', models.CharField(max_length=15, verbose_name='Padre quien Bautizo')),
                ('persona', models.OneToOneField(to='persona.Persona')),
            ],
            options={
                'db_table': 'bautismo',
                'verbose_name': 'Registro de Bautismo',
                'verbose_name_plural': 'Registro de Bautismos',
            },
        ),
    ]
