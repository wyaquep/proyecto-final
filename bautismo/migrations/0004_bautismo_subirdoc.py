# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bautismo', '0003_remove_bautismo_subir'),
    ]

    operations = [
        migrations.AddField(
            model_name='bautismo',
            name='subirdoc',
            field=models.FileField(default=1, upload_to='documentos'),
            preserve_default=False,
        ),
    ]
