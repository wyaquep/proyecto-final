# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bautismo', '0002_bautismo_subir'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bautismo',
            name='subir',
        ),
    ]
