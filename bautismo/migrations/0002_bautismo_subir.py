# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bautismo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bautismo',
            name='subir',
            field=models.FileField(default=1, storage='fs', upload_to=b''),
            preserve_default=False,
        ),
    ]
