from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseNotFound
from bautismo.models import Bautismo


def resume(request):

    #Get the applicant's resume
    resume = Bautismo.objects.get(bautismo=Bautismo)
    fsock = open(resume.location, 'r')
    response = HttpResponse(fsock, mimetype='application/pdf')

    return response
