# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('persona', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Confirmacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('libro', models.IntegerField(verbose_name='No. Libro')),
                ('folio', models.IntegerField(verbose_name='No. Folio')),
                ('fconfirmacion', models.DateField(verbose_name='Fecha de Confirmaci\xf3n')),
                ('monsenor', models.CharField(max_length=15, verbose_name='Monse\xf1or quien Confirmo')),
                ('nombrepadrino', models.CharField(max_length=30, verbose_name='Nombres del Padrino')),
                ('apellidopadrino', models.CharField(max_length=30, verbose_name='Apellidos del Padrino')),
                ('nombremadrina', models.CharField(max_length=30, verbose_name='Nombres de Madrina')),
                ('apellidomadrina', models.CharField(max_length=30, verbose_name='Apellidos de Madrina')),
                ('persona', models.OneToOneField(to='persona.Persona')),
            ],
            options={
                'db_table': 'confirmacion',
                'verbose_name': 'Registro de Confirmacion',
                'verbose_name_plural': 'Registro de Confirmacion',
            },
        ),
    ]
