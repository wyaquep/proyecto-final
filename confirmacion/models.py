# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from persona.models import Persona

class Confirmacion(models.Model):
    libro = models.IntegerField('No. Libro')
    folio = models.IntegerField('No. Folio')
    persona = models.OneToOneField(Persona)
    fconfirmacion = models.DateField('Fecha de Confirmación')
    monsenor = models.CharField('Monseñor quien Confirmo', max_length=15)
    nombrepadrino = models.CharField('Nombres del Padrino', max_length=30)
    apellidopadrino = models.CharField('Apellidos del Padrino', max_length=30)
    nombremadrina = models.CharField('Nombres de Madrina', max_length=30)
    apellidomadrina = models.CharField('Apellidos de Madrina', max_length=30)

    def __unicode__(self):
        return '%s  %s' % (self.nombre, self.apellido)

    class Meta:
        db_table = 'confirmacion'
        verbose_name = 'Registro de Confirmacion'
        verbose_name_plural = 'Registro de Confirmacion'
