# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

GENERO_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)

class Persona(models.Model):
    nombre = models.CharField('Nombres', max_length=30)
    apellido = models.CharField('Apellidos', max_length=30)
    fechanac = models.DateField('Fecha de Nacimiento')
    genero = models.CharField(
        'Genero', max_length=1,
        choices=GENERO_CHOICES, default='M')
    nombrepadre = models.CharField('Nombres del Padre', max_length=30)
    apellidopadre = models.CharField('Apellidos del Padre', max_length=30)
    nombremadre = models.CharField('Nombres de la Madre', max_length=30)
    apellidomadre = models.CharField('Apellidos de la Madre', max_length=30)

    def __unicode__(self):
        return '%s  %s' % (self.nombre, self.apellido)

    class Meta:
        db_table = 'persona'
        verbose_name = 'Registro de Persona'
        verbose_name_plural = 'Registro de Personas'
