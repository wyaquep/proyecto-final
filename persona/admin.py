# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
from bautismo.models import Bautismo
from confirmacion.models import Confirmacion
#from comunion.models import Comunion
from matrimonio.models import Matrimonio
from jet.admin import CompactInline

# Register your models here.

class BautismosInline(CompactInline):
    model = Bautismo
    extra = 0
    max_num = 1

class ConfirmacionInline(CompactInline):
    model = Confirmacion
    extra = 0
    max_num = 1

class MatrimonioInline(CompactInline):
    model = Matrimonio
    extra = 0
    max_num = 1

class PersonaAdmin(admin.ModelAdmin):
    inlines = [BautismosInline, ConfirmacionInline, MatrimonioInline]

    search_fields = ['nombre', 'apellido']

admin.site.register(Persona, PersonaAdmin)
