# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=30, verbose_name='Nombres')),
                ('apellido', models.CharField(max_length=30, verbose_name='Apellidos')),
                ('fechanac', models.DateField(verbose_name='Fecha de Nacimiento')),
                ('genero', models.CharField(default='M', max_length=1, verbose_name='Genero', choices=[('M', 'Masculino'), ('F', 'Femenino')])),
                ('nombrepadre', models.CharField(max_length=30, verbose_name='Nombres del Padre')),
                ('apellidopadre', models.CharField(max_length=30, verbose_name='Apellidos del Padre')),
                ('nombremadre', models.CharField(max_length=30, verbose_name='Nombres de la Madre')),
                ('apellidomadre', models.CharField(max_length=30, verbose_name='Apellidos de la Madre')),
            ],
            options={
                'db_table': 'persona',
                'verbose_name': 'Registro de Persona',
                'verbose_name_plural': 'Registro de Personas',
            },
        ),
    ]
